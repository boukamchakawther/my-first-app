//Import needed libraries
import { AppRegistry } from 'react-native';
import App from './src/App';

//Rendering on the device
//Must register at least 1 component
AppRegistry.registerComponent('AwesomeProject', ()=> App);

