import React, {component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

class Header extends Component  {
   render(){
  return (
    <View>
      <Text style={styles.homeStyle}>
          {this.props.children}
          </Text>
    </View>
  );
}
}
const styles = StyleSheet.create({
  homeStyle: {
    backgroundColor: 'white',
    textAlign: 'center',
    fontSize: 35,
    color: 'blue',
  },
});

export default Header;
