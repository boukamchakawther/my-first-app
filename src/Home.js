import React from 'react';
import {
  StyleSheet,
  Button,
  View,
  SafeAreaView,
  Text,
  Alert,
  ImageBackground,
} from 'react-native';

const Separator = () => <View style={styles.separator} />;

const Home = ({navigation}) => (
  <SafeAreaView style={styles.container}>
    <View>
      <Text style={styles.homeStyle}>Hello kawther !</Text>
    </View>
    <Separator />
    <View>
      <Text style={styles.title}>
        All interaction for the component are disabled.
      </Text>
      <Button
        title="Press me"
        disabled
        onPress={() => Alert.alert('Cannot press this one')}
      />
    </View>
    <Separator />
    <View>
      <Text style={styles.title}>
        This layout strategy lets the title define the width of the button.
      </Text>
      <View style={styles.fixToText}>
      <Button
        title="Login"
        onPress={() => navigation.navigate('LoginForm')}
      />
    
        <Button 
        style= {styles.design}
          title="Log out"
          onPress={() => Alert.alert('Right button pressed')}
        />
      </View>
    </View>
  </SafeAreaView>
);

const styles = StyleSheet.create({
  homeStyle: {
    textAlign: 'center',
    fontSize: 35,
    color: 'white',
  },

  container: {
    backgroundColor: '#9FA8DA',
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 0,
  },
  title: {
    textAlign: 'center',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  separator: {
    marginVertical: 20,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  design: {
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'rgba(127, 220, 103, 1)' 
  },
});

export default Home;

