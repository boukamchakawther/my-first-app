import React, { Component } from 'react';
//Import needed libraries

import {View, Text, Button} from 'react-native';

//Create Component
function LoginForm({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>LoginForm</Text>
      <Button
        title="Go to Home"
        onPress={() => navigation.navigate('Home')}
      />
    </View>
  );
}

export default LoginForm;
