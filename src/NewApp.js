import * as React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import Home from './Home';

import LoginForm from './LoginForm';

const Drawer = createDrawerNavigator();

export default function NewApp() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={Home} />
        <Drawer.Screen name="LoginForm" component={LoginForm} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
